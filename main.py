import time


# Необходимо реализовать менеджер контекста, печатающий на экран:
#
# Время запуска кода в менеджере контекста;
# Время окончания работы кода;
# Сколько было потрачено времени на выполнение кода.

class Timer:
    def __init__(self):
        self.start_time = time.time()
        print(f'Время начала: {self.start_time}')

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end_time = time.time()
        print(f'Время окончания: {self.end_time}')
        print(f'На выполнение кода было затрачено: {self.end_time - self.start_time} секунд')


# Придумать и написать программу, использующая менеджер контекста из задания 1.
# Если придумать не получиться, использовать программу из предыдущих домашних работ.


with Timer() as t:
    def read_cook_book_file():
        with open("recipes.txt") as f:
            cook_book = dict()

            for line in f:
                line = line.strip()
                cook_book[line] = []
                count_ingredient = int(f.readline())
                count = 0

                while count < count_ingredient:
                    ingredient = f.readline().strip().split('|')
                    cook_book[line].append({
                        "ingredient_name": ingredient[0].strip(),
                        "quantity": int(ingredient[1].strip()),
                        "measure": ingredient[2].strip(),
                    })
                    count += 1

                f.readline()

        return cook_book


    def get_shop_list_by_dishes(dishes, person_count):
        shop_list = dict()

        for dish in dishes:
            ingredients = cook_book[dish]

            for ingredient in ingredients:
                if ingredient["ingredient_name"] in shop_list:
                    shop_list[ingredient["ingredient_name"]]["quantity"] += (ingredient["quantity"] * person_count)
                else:
                    shop_list[ingredient["ingredient_name"]] = {
                        "measure": ingredient["measure"],
                        "quantity": ingredient["quantity"] * person_count
                    }

        print(shop_list)


    cook_book = read_cook_book_file()
    get_shop_list_by_dishes(['Омлет', 'Утка по-пекински', 'Фахитос'], 14)

